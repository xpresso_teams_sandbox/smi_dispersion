"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
import pandas as pd
import numpy as np
from datetime import timedelta, datetime
import matplotlib.pyplot as plt
import datetime

__author__ = "### Author ###"

logger = XprLogger("run_calcs",level=logging.INFO)


class RunCalcs(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="RunCalcs")
        """ Initialize all the required constansts and data her """
    
    
    def calcVol(self,returns, lag, nexpected, vol_ann=252):
        n = returns.shape[0] - lag
        sum_sq = (returns**2).sum()
        nexp=nexpected
        Vol = np.sqrt((vol_ann/(nexp * lag)) * sum_sq)
        return Vol, n
    
    
    
    def calcIndexReturns(self,data, lag, dates_dict):
        data.dropna()
        returns_all = []
        for sd, ed in dates_dict.items():
            returns = (np.log(data.loc[sd:ed]/data.loc[sd:ed].shift(lag))).dropna() # Calculate weighted basket return with date range
            returns_all.append(returns)
        return returns_all
    
    def calcDates(self,data):
        dates_dict = dict()
        date_delta = timedelta(days=270) # Timedelta 9 months assuming average of 30 days a month
        dates = data.index
        #print(dates[0:5])
        #print(dates[-5:])
        for date_start in dates:
            date_end = date_start + date_delta
            if date_end > dates[-1]:
                break
            while (date_end not in dates):
                date_end = date_end + timedelta(days=1)
            dates_dict[date_start] = date_end # Dictionary of dates with start date the key and end date the value
        return dates_dict
    
    
    def volSwapPayoff(self,vega, vol_realised, vol_strike, cap):
        return 100* vega * (min(vol_realised, cap) - vol_strike)
    
    
    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            
            baseprices=pd.read_csv("/data/smi_dispersion_stockdata.csv",index_col=0)
            basedata=pd.read_csv("data/smi_dispersion_basedata.csv")
            
            baseprices=baseprices.dropna().copy(deep=True)
            
            datestrings = [str(x) for x in baseprices['Date']]
            
            #'convert to datetime'
            baseprices['Date']=[(datetime.datetime.strptime(x, '%Y-%m-%d')).date() for x in datestrings]
            
            baseprices=baseprices.set_index('Date')
            

            col=list(baseprices.columns)
            for col in baseprices:
                baseprices[col]=pd.to_numeric(baseprices[col], errors='coerce')
        
            
            list_of_stocks=list(basedata['BBG Ticker'])
            
            list_of_weights=list(basedata['Weight'])
            
            list_of_strikes=list(basedata['Vol Strike'])
            
            list_of_vega=basedata['VegaAmount']
            
            list_of_caps=basedata['Cap Level']
            
            list_of_nexp=basedata['Expected N']
            
            list_of_strikes=pd.to_numeric(list_of_strikes, errors='coerce')
            list_of_vega=pd.to_numeric(list_of_vega, errors='coerce')
            list_of_caps=pd.to_numeric(list_of_caps, errors='coerce')
            list_of_nexp=pd.to_numeric(list_of_nexp, errors='coerce')
            
            universe = list_of_stocks 
            
            weights = list_of_weights
            varStrikes = list_of_strikes #'[0.285,0.285,0.305]


            data = baseprices 
            
            basket_data = data.dropna(axis=0, how='any') 
            
                               
            # dates 
            dates_dict = self.calcDates(basket_data)
            start_dates = list(dates_dict.keys())
            end_dates = list(dates_dict.values())
            
            results = pd.DataFrame([], index=end_dates)
            
            
            ### INDIVIDUAL INDICES ###
            # Returns
            index_returns_all = []
            tickers = list(data.columns)
            for column in data:
                print(column)
                index_returns = self.calcIndexReturns(data[column], 1, dates_dict)
                index_returns_all.append(index_returns)
            # Variance
            i=0
            for index in index_returns_all:
                index_vol_all = []
                index_n_all = []
                print(i)
                for ret in index:

                    (index_vol, index_n) = self.calcVol(ret, 1, list_of_nexp[i], 252)
                    index_vol_all.append(index_vol)
                    index_n_all.append(index_n)
                results[tickers[i] + '_n'] = index_n_all
                results[tickers[i] + '_vol'] = index_vol_all
                # Swap Payoff
                index_swap_payoff_all = []
                for vol in index_vol_all:
                    index_swap_payoff = self.volSwapPayoff(list_of_vega[i], vol, varStrikes[i], list_of_caps[i])
                    index_swap_payoff_all.append(index_swap_payoff)
                results[tickers[i] + '_swap_payoff'] = index_swap_payoff_all
                i+=1
            
            # Strategy payoff
            columns = []
            for ticker in tickers:
                columns.append(ticker + '_swap_payoff')
            total_index_payoff = (results[columns]).sum(axis=1)
            
            total_payoff = total_index_payoff 
            # total_payoff_theta = total_index_payoff_theta - results['basket_swap_payoff']
            results['total_payoff'] = total_payoff
            results['TRS Level'] =  100 + (total_payoff/500000)
            
            
            # Realised VolSpread
            columns2 = []
            for ticker in tickers:
                columns2.append(ticker + '_vol')
                
            #remove SPX
            columns2=columns2[0:-1]
            weights=weights[0:-1]
               
            index_weighted_vol = ((results[columns2])*weights).sum(axis=1)
            vol_spread = index_weighted_vol - results['SWISSMI_vol']
            
            results['Wtd SS Vol'] = index_weighted_vol
            results['vol_spread'] = vol_spread * 100
            

            
            graphdf=results
            
            graphdf[["total_payoff"]].plot()
            plt.savefig("/data/SMI_dispersion_PnL.png")
            graphdf[['TRS Level' ]].plot()
            print(graphdf[['TRS Level' ]])
            plt.savefig("/data/SMI_dispersion_TRS.png")
            graphdf[['vol_spread' ]].plot()
            plt.savefig("/data/SMI_dispersion_Volspread.png")      
            
            
            
            
            
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = RunCalcs()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
